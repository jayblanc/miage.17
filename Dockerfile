FROM jboss/wildfly:10.1.0.Final

RUN /opt/jboss/wildfly/bin/add-user.sh admin Admin#70365 --silent

WORKDIR $JBOSS_HOME

COPY standalone.xml ./standalone/configuration/filestore.xml
COPY ear/target/filestore-ear.ear ./standalone/deployments/

CMD ["/opt/jboss/wildfly/bin/standalone.sh", "-c", "filestore.xml", "-b", "0.0.0.0", "-bmanagement", "0.0.0.0"]
