package fr.miage.filestore.file;

public interface FileServiceMetrics {

    int getTotalUploads() throws FileServiceException;

    int getTotalDownloads() throws FileServiceException;

    int getUptime() throws FileServiceException;

}
