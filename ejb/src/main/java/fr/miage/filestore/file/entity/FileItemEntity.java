package fr.miage.filestore.file.entity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@NamedQueries({
		@NamedQuery(name="listAllFiles", query="SELECT fi FROM FileItemEntity fi")
})
public class FileItemEntity {

	@Id
	private String id;
	private String name;
	private String type;
	private long length;
	private long nbdownloads;
	private String owner;
	@ElementCollection
	private Set<String> receivers;
	@Column(length = 5000)
	private String message;
	@Temporal(TemporalType.TIMESTAMP)
	private Date creation;
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastdownload;
	private String stream;

	public FileItemEntity() {
		nbdownloads = 0;
		creation = new Date();
		lastdownload = creation;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public long getLength() {
		return length;
	}

	public void setLength(long length) {
		this.length = length;
	}

	public long getNbdownloads() {
		return nbdownloads;
	}

	public void setNbdownloads(long nbdownloads) {
		this.nbdownloads = nbdownloads;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public Set<String> getReceivers() {
		return receivers;
	}

	public void setReceivers(Set<String> receivers) {
		this.receivers = receivers;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Date getCreation() {
		return creation;
	}

	public void setCreation(Date creation) {
		this.creation = creation;
	}

	public Date getLastdownload() {
		return lastdownload;
	}

	public void setLastdownload(Date lastdownload) {
		this.lastdownload = lastdownload;
	}

	public String getStream() {
		return stream;
	}

	public void setStream(String stream) {
		this.stream = stream;
	}

}
