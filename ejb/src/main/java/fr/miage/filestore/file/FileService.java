package fr.miage.filestore.file;

import fr.miage.filestore.file.entity.FileItemEntity;

import java.io.InputStream;
import java.util.Set;


public interface FileService {
	
	String postFile(String owner, Set<String> receivers, String message, String name, InputStream stream) throws FileServiceException;
	
	FileItemEntity getFile(String id) throws FileServiceException;

	InputStream getFileContent(String id) throws FileServiceException;

	void deleteFile(String id) throws FileServiceException;

}
