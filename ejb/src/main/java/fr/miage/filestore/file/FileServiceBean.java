package fr.miage.filestore.file;

import fr.miage.filestore.file.entity.FileItemEntity;
import fr.miage.filestore.file.metrics.FileServiceMetricsBean;
import fr.miage.filestore.store.BinaryStoreService;
import fr.miage.filestore.store.BinaryStoreServiceException;
import fr.miage.filestore.store.BinaryStreamNotFoundException;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.jms.JMSContext;
import javax.jms.Topic;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.InputStream;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

@Stateless
@Interceptors({FileServiceMetricsBean.class})
public class FileServiceBean implements FileService {

    private static final Logger LOGGER = Logger.getLogger(FileService.class.getName());

    @PersistenceContext(unitName="filestore-pu")
    protected EntityManager em;

    @EJB
    protected BinaryStoreService store;

    @Resource(mappedName = "java:jboss/exported/jms/topic/Notification")
    private Topic notificationTopic;
    @Inject
    private JMSContext jmsctx;

    @Override
    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public String postFile(String owner, Set<String> receivers, String message, String name, InputStream stream) throws FileServiceException {
        LOGGER.log(Level.FINE, "Posting File");
        try {
            String streamId = store.put(stream);
            String id = UUID.randomUUID().toString();
            FileItemEntity fileItemEntity = new FileItemEntity();
            fileItemEntity.setId(id);
            fileItemEntity.setOwner(owner);
            fileItemEntity.setReceivers(receivers);
            fileItemEntity.setMessage(message);
            fileItemEntity.setName(name);
            fileItemEntity.setStream(streamId);
            em.persist(fileItemEntity);
            this.notify(owner, receivers, id, message);
            return id;
        } catch (BinaryStoreServiceException e) {
            throw new FileServiceException("unable to persist file stream", e);
        }
    }

    @Override
    @TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
    public FileItemEntity getFile(String id) throws FileServiceException {
        FileItemEntity item = em.find(FileItemEntity.class, id);
        if ( item == null ) {
            throw new FileServiceException("Unable to get file with id '" + id + "' : file does not exists");
        }
        return item;
    }

    @Override
    @TransactionAttribute(value = TransactionAttributeType.SUPPORTS)
    public InputStream getFileContent(String id) throws FileServiceException {
        FileItemEntity item = getFile(id);
        try {
            InputStream stream = store.get(item.getStream());
            return stream;
        } catch (BinaryStreamNotFoundException e) {
            throw new FileServiceException("unable to find stream for file with id: " + id, e);
        } catch (BinaryStoreServiceException e) {
            throw new FileServiceException("error while trying to get file stream", e);
        }
    }

    @Override
    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public void deleteFile(String id) throws FileServiceException {
        FileItemEntity item = em.find(FileItemEntity.class, id);
        if ( item == null ) {
            throw new FileServiceException("Unable to delete file with id '" + id + "' : file does not exists");
        }
        em.remove(item);
        try {
            store.delete(item.getStream());
        } catch (BinaryStoreServiceException e) {
            LOGGER.log(Level.WARNING, "File content not deleted for key: " + item.getStream());
        }
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    private void notify(String owner, Set<String> receivers, String id, String message) throws FileServiceException {
        try {
            javax.jms.Message msg = jmsctx.createMessage();
            msg.setStringProperty("owner", owner);
            StringBuilder receiversBuilder =  new StringBuilder();
            for ( String receiver : receivers ) {
                receiversBuilder.append(receiver).append(",");
            }
            receiversBuilder.deleteCharAt(receiversBuilder.lastIndexOf(","));
            msg.setStringProperty("receivers", receiversBuilder.toString());
            msg.setStringProperty("id", id);
            msg.setStringProperty("message", message);
            jmsctx.createProducer().send(notificationTopic, msg);

        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "unable to notify", e);
            throw new FileServiceException("unable to notify", e);
        }
    }
}
